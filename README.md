# Youtube Video to Invidious

Source code for the 'Youtube Video to Invidious' Firefox extension.

Annoyed by YouTube not letting you watch videos with an adblocker on? This very simple extension redirects YouTube videos to its Invidious equivalent, esentially bypassing YouTube's shenanigans.

Unlike other Invidious extensions, this one lets you browse YouTube like usual, only redirecting to invidious when the extension detects that you are on a YouTube video. This way you can still use your YouTube subscriptions and the YouTube algorithm.

The redirect is made in a kinda weird way, but hey, it works!
