const getting = browser.storage.sync.get("inv_url");
getting.then(onGot, onError);

function onError(error) {
  console.log(`Error: ${error}`);
}

function onGot(item) {
  let inv_url = "invidious.flokinet.to";
  if (item.inv_url) {
    inv_url = item.inv_url;
  }
  waitFor(_ => window.location.href.includes("youtube.com/watch?v=") === true)
    .then(_ => window.location = "https://" + inv_url + "/watch?v=" + window.location.href.split("/watch?v=")[1]);
}

function waitFor(conditionFunction) {

    const poll = resolve => {
      if(conditionFunction()) resolve();
      else setTimeout(_ => poll(resolve), 400);
    }
  
    return new Promise(poll);
}
