function saveOptions(e) {
e.preventDefault();
browser.storage.sync.set({
    inv_url: document.querySelector("#inv_url").value,
});
}

function restoreOptions() {
function setCurrentChoice(result) {
    document.querySelector("#inv_url").value = result.inv_url || "invidious.flokinet.to";
}

function onError(error) {
    console.log(`Error: ${error}`);
}

let getting = browser.storage.sync.get("inv_url");
getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
  